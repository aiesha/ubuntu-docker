Run Ubuntu GUI apps via x11 and mac  

 xhost + #to allow remote connections to x11
 XQuartz for mac
  

#BUILD
docker build -t ubuntu .

#RUN
docker run  --privileged --rm -it --net=host --security-opt seccomp:unconfined   --user ubuntu -e DISPLAY=host.docker.internal:0 -v $HOME/.Xauthority:/root/.Xauthority  -v /Users/burnick/Development/ubuntu/downloads:/home/ubuntu/downloads ubuntu  bash

#to run sample gui app

firefox ${some-website-url}

#sample command

transmission-cli  --download-dir ~/downloads

