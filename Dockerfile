FROM ubuntu:18.04

ARG group=ubuntu
ARG user=ubuntu
ARG uid=1000
ARG gid=1000

RUN  apt-get update && \
     apt-get -qq -y upgrade && \
     apt-get install -qq -y transmission-cli  firefox  vim sudo && \
     rm -rf /var/lib/apt/lists/*

USER root
# Create home directory
WORKDIR /home/${user}

# Add a new group & user 
RUN addgroup --gid ${gid} --system ${group} 
RUN adduser \
    --disabled-password \
    --ingroup "$group" \
    #--no-create-home \
    --uid "$uid" \
    --gecos "" \
    "$user" && echo "${user}:${user}" | chpasswd && adduser ${user} sudo

RUN echo "net.core.rmem_max = 4194304" >> /etc/sysctl.conf

#RUN chown -R $user:$user /usr/src/app
RUN mkdir /home/${user}/downloads
RUN chown -R $user:$user /home/${user}

#Change to non-root privilege
USER $user

CMD /bin/bash

